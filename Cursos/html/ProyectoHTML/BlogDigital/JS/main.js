// Get the 
var modal = document.getElementById('newArticle');

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

// Function to order articles per date
function ordenar(){
    let divArticlesSection = document.getElementById('listArticles');
    let listSections = document.querySelectorAll('#listArticles > section');
    let listArraySections = Array.prototype.slice.call(listSections, 0);
    listArraySections.sort( (a,b) => {
        return new Date(a.children[0].children[0].children[3].textContent) - new Date(b.children[0].children[0].children[3].textContent);
    });
    listArraySections.forEach( htmlElementSection => divArticlesSection.appendChild(htmlElementSection) );
} 

// Function to create a new article
function crear(title, description, date) {
    const image = document.createElement('img');
    let divArticlesSection = document.getElementById('listArticles');
    let section = document.createElement('section');
    let fieldset = document.createElement('fieldset');
    let article = document.createElement('article');
    let h2 = document.createElement('h2');
    let p = document.createElement('p');
    let small = document.createElement('small');
    let br = document.createElement('br');
    image.src = URL.createObjectURL(img.files[0]);
    image.style.width = '250px';
    image.style.height = '200px';
    h2.innerHTML = title.value;
    p.innerHTML = description.value;
    small.innerHTML = date.value;
    fieldset.appendChild(image);
    fieldset.appendChild(h2);
    fieldset.appendChild(p);
    fieldset.appendChild(small);
    article.appendChild(fieldset);
    article.appendChild(br);
    section.appendChild(article);
    divArticlesSection.appendChild(section);
    ordenar();
    document.getElementById("form").reset();
    document.getElementById('newArticle').style.display='none';
}


// Order the articles per date
ordenar();