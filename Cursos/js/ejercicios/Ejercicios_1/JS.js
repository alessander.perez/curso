function ejercicio1() {
    function f(x, y=2, z=7) {
        return x + y + z;
    }
    console.log(f(5, undefined));
    let p = document.getElementById('default');
    p.innerHTML = f(5, undefined);
}

function ejercicio2() {
    var animal = 'kitty';
    var result = (animal === 'kitty') ? 'cute' : 'still nice';
    console.log(result);
    let p = document.getElementById('ternary');
    p.innerHTML = result;
}

function ejercicio3() {
    var animal = 'kitty';
    var result = '';
    if (animal === 'kitty') {
        result = 'cute';
    } else {
        result = 'still nice';
    }
    console.log(result);
    let p = document.getElementById('conditional');
    p.innerHTML = result;
}

function ejercicio4() {
    var a = 0;
    var str = 'not a';
    var b = '';
    b = a === 0 ? (a = 1, str += ' test') : (a = 2);
    console.log(b);
    let p = document.getElementById('ternary2');
    p.innerHTML = `a = ${a}, b = ${b}`;
}

function ejercicio5() {
    var a = 1;
    a === 1 ? alert('Hey, it is 1!') : 0;
    console.log(a);
    let p = document.getElementById('ternary3');
    p.innerHTML = 'Hey it is 1!';
}

function ejercicio6() {
    var a = ' whatever';
    a === 1 ? alert('Hey, it is 1!') : alert('Weird, what could it be?');
    if ( a === 1 ) alert('Hey it is 1!'); else alert('Weird, what could it be?');
    console.log(a);
    let p = document.getElementById('ternary4');
    p.innerHTML = 'Weird, what could it be?';
}

function ejercicio7() {
    var animal = 'kitty';
    for( var i = 0; i < 5; ++i ) {
        if (animal === 'kitty') break; else console.log(i);
    }
    console.log('expression expected');
    let p = document.getElementById('conditionalTernary');
    p.innerHTML = 'Error: expression expected';
}

function ejercicio8() {
    var value = 1;
    let result;
    switch (value) {
        case 1 : 
            console.log('I will always run');
            result = 'I will always run';
            break; 
        case 2 : 
            console.log('I will never run');
            result = 'I will never run';
            break;
    }
    let p = document.getElementById('switch');
    p.innerHTML = result;
}

function ejercicio9() {
    var animal = 'lion';
    let result;
    switch (animal) {
        case 'Dog' : 
            console.log('I will not run sice animal !== "Dog"');
            result = 'I will not run sice animal !== "Dog"';
            break; 
        case 'Cat' : 
            console.log('I will not run sice animal !== "Cat"');
            result = 'I will not run sice animal !== "Cat"';
            break;
        default : 
            console.log('I will run sice animal does not match any other case');
            result = 'I will run sice animal does not match any other case';
    }
    let p = document.getElementById('switch2');
    p.innerHTML = result;
}

function ejercicio10() {
    let result = 'No entra a ningun caso puesto que name no esta definido';
    function john() {
        return 'John';
    }
    function jacob(){
        return 'Jacob';
    }
    switch (name) {
        case john() : 
            console.log('I will run if name === "John"');
            result = 'I will run if name === "John"';
            break; 
        case 'Ja' + 'ne' : 
            console.log('I will run if name === "Jane"');
            result = 'I will run if name === "Jane"';
            break;
        case john() + ' ' + jacob() + ' Jingleheimer Schmidt': 
            console.log('His name is equal to me too!');
            result = 'His name is equal to me too!';
    }
    let p = document.getElementById('switch3');
    p.innerHTML = result;
}

function ejercicio11() {
    let result = '';
    let x = "c";
    switch (x) {
        case "a" :
        case "b" :
        case "c" : 
            console.log('Either a, b, or c was selected.');
            result = 'Either a, b, or c was selected.';
            break; 
        case "d": 
            console.log('Only d was selected.');
            result = 'Only d was selected.';
            break;
        default : 
            console.log('No case was matched.');
            result = 'No case was matched.';
            break;
    }
    let p = document.getElementById('switch4');
    p.innerHTML = result;
}

function ejercicio12() {
    let result = `${5+7}, ${5+"7"}, ${5-7}, ${5 - "7"}, ${5 - "7"}, ${5 - "x"}.`;
    console.log(result);
    let p = document.getElementById('operadores');
    p.innerHTML = result;
}

function ejercicio13() {
    let result = `${'hello' || ''}, ${'' || []}, ${'' || undefined}, ${1 || 5}, ${0 || {}}, ${0 || '' || 5}, ${'' || 'yay' || 'boo'}.`;
    console.log(result);
    let p = document.getElementById('operadoresLogicos');
    p.innerHTML = result;
}

function ejercicio14() {
    let result = `${'hello' && ''}, ${'' && []}, ${undefined && 0}, ${1 && 5}, ${0 && {}}, ${'hi' && [] && 'done'}, ${'bye' && undefined && 'adios'}.`;
    console.log(result);
    let p = document.getElementById('operadoresLogicos2');
    p.innerHTML = result;
}

function ejercicio15() {
    var foo = function(val) {
        return val || 'default';
    }

    let result = `${foo('burger')}, ${foo(100)}, ${foo([])}, ${foo(0)}, ${foo(undefined)}.`;
    console.log(result);
    let p = document.getElementById('operadoresLogicos3');
    p.innerHTML = result;
}

function ejercicio16() {
    // let result = `${age >= 18}, ${height >= 5.11}, ${isLegal && tall}, ${status === 'royalty'}, ${isRoyalty && hasInvitation}, ${suitable || specialCase}.`;
    // console.log(result);
    let p = document.getElementById('operadoresLogicos4');
    p.innerHTML = 'age is not defined';
}

function ejercicio17() {
    let result = ''
    for (var i = 0; i < 3; i++) {
        if(i === 1 ){
            continue;
        }
        console.log(i);
        result += i + ' '; 
    }
    let p = document.getElementById('continue');
    p.innerHTML = result;
}

function ejercicio18() {
    var i = 0;
    let result = '';
    while (i < 3) {
        if ( i === 1 ) {
            i = 2;
            continue;
        }
        console.log(i);
        result += i + ' ';
        i++;
    }
    let p = document.getElementById('continue2');
    p.innerHTML = result;
}

function ejercicio19() {
    let result = ''
    for (var i = 0; i < 5; i++) {
        for (var j=0; j < 5; j++) {
            if(i == j) break;
            console.log(i, j);
            result += i + ' ' + j + ' ';
        } 
    }
    let p = document.getElementById('break');
    p.innerHTML = result;
}

function ejercicio20() {
    function foo() {
        var a = 'hello';
        function bar() {
            var b = 'world';
            console.log(a);
            console.log(b);
        }
        console.log(a);
        console.log(b);
    }
    let p = document.getElementById('nestedFunction');
    p.innerHTML = 'a is not defined';
}

function ejercicio21() {
    function foo() {
        const a = true;
        function bar() {
            const a = false;
            console.log(a);
        }
        // const a = false;
        // a = false;
        console.log(a);
    }
    
    foo();
    let p = document.getElementById('nestedFunction2');
    p.innerHTML = 'a has already been declared';
}

function ejercicio22() {
    let result = '';
    var nameSum = function sum (a, b) {
        return a + b;
    }
    var anonSum = function (a,b) {
        return a + b;
    }
    console.log(nameSum(1,3));
    console.log(anonSum(1,3));
    let p = document.getElementById('functionExpressions');
    result += nameSum(1,3) + ' ' + anonSum(1,3) + '';
    p.innerHTML = result;
}

function ejercicio23(){
    let result = '';
    var a = [1,2,3,8,9,10];
    a = a.slice(0,3).concat([4,5,6,7], a.slice(3,6));
    var b = [1,2,3,8,9,10];
    b = b.splice(3,0,...[4,5,6,7]);
    console.log(a, b);
    result += a + ' ' + b;
    let p = document.getElementById('arrayMethods');
    p.innerHTML = result;
}

function ejercicio24() {
    var array = ['a', 'b', 'c'];
    var joinArrow = array.join('->');
    var joinDot = array.join('.');
    var splitDot = 'a.b.c'.split('.');
    var splitDot2 = '5.4.3.2.1'.split('.');
    let p = document.getElementById('arrayMethods2');
    console.log(`${joinArrow}, ${joinDot}, ${splitDot}, ${splitDot2}`);
    p.innerHTML = `${joinArrow}, ${joinDot}, ${splitDot}, ${splitDot2}`;
}

function ejercicio25() {
    var array = [5, 10, 15, 20, 25];
    result = Array.isArray(array) + ' ' + array.includes(10), + ' ' + array.includes(10, 2) + ' ' + array.indexOf(25) + ' ' + array.lastIndexOf(10, 0);
    console.log(result);
    let p = document.getElementById('arrayMethods3');
    p.innerHTML = result;
}

function ejercicio26() {
    var array = ['a', 'b', 'c', 'd', 'e', 'f'];
    array.copyWithin(5,0,1);
    array.copyWithin(3,0,3);
    array.fill('Z', 0, 5);
    console.log(array);
    var array2 = ['Alberto', 'Ana', 'Mauricio', 'Bernardo', 'Zoe'];
    array2.sort();
    array2.reverse();
    console.log(array2);
    let result = array + ' ' + array2;
    let p = document.getElementById('arrayMethods4');
    p.innerHTML = result;
}

ejercicio1();
ejercicio2();
ejercicio3();
ejercicio4();
ejercicio5();
ejercicio6();
ejercicio7();
ejercicio8();
ejercicio9();
ejercicio10();
ejercicio11();
ejercicio12();
ejercicio13();
ejercicio14();
ejercicio15();
ejercicio16();
ejercicio17();
ejercicio18();
ejercicio19();
ejercicio20();
ejercicio21();
ejercicio22();
ejercicio23();
ejercicio24();
ejercicio25();
ejercicio26();