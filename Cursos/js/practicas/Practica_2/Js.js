function divide(dividiendo, divisor) {
    return new Promise((resolve, reject) => {
        if(divisor === 0 ){
            reject(new Error('No se puede dividir entre 0'));
        } else {
            resolve(dividiendo/divisor);
        }
    });
}

try {
    const result = divide(5,1);
    console.log(result);
} catch (err) {
    console.error(err.menssaje);
}