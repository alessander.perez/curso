function innerHtmlText(id, array) {
    let tag = document.getElementById(id);
    tag.innerHTML = array.join(', ');
}


function ejercicio2() {
    var numbers = [5,32,43,4];
    var imparArray = numbers.filter( n => n % 2 !== 0 );
    innerHtmlText('ejercicio2', imparArray);
}

function ejercicio3() {
    var people = [
        {
            id: 1,
            name: "John",
            age: 28
        }, {
            id: 2,
            name: "Jane",
            age: 31
        }, {
            id: 3,
            name: "Peter",
            age: 55
        }
    ];
    let namesArray = people.filter( obj => obj.age < 35);
    innerHtmlText('ejercicio3', namesArray.map(obj => obj.name));
}

function ejercicio4(){
    let people = [
        {
            id: 1,
            name: "Bob"
        }, {
            id: 2,
            name: "John"
        }, {
            id: 3,
            name: "Alex"
        }, {
            id: 3,
            name: "John"
        }
    ];
    // Se hara uso de mapas para resolver el problema.
    let map = new Map();
    for (let i = 0; i<people.length; i++) {
        if (map.has(people[i].name)) map.set(people[i].name, map.get(people[i].name) + 1); 
            else map.set(people[i].name, 1);
    }
    let array = Array.from(map).map( array => array.join(': '));
    innerHtmlText('ejercicio4', array)
}

function ejercicio5() {
    var myArray = [1,2,3,4];
    let array = [myArray[0], myArray[0]];
    myArray.forEach( n => {
        if (array[0] < n ) array[0] = n;
        if (array[1] > n) array[1] = n; 
    });
    innerHtmlText('ejercicio5', array);
}

function ejercicio6() {
    var object = {
        key1: 10,
        key2: 3,
        key3: 40,
        key4: 20
    };
    let array = Object.values(object);
    array.sort((a,b) => a-b );
    innerHtmlText('ejercicio6', array);
}

ejercicio2();
ejercicio3();
ejercicio4();
ejercicio5();
ejercicio6();