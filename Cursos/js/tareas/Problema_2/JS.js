let crear = document.getElementById('crear');
let eliminar = document.getElementById('eliminar');
let divTable = document.getElementById('divTable');
let list = document.querySelectorAll('li');

let header = [{
    "personID" : 'ID',
    "name" : "NAME",
    "city" : "CITY",
    "phoneNo" : "PHONE"
}];
let personArray = [
    {
        "personID" : 123,
        "name" : "John",
        "city" : "Melbourne",
        "phoneNo" : "1234567890"
    },
    {
        "personID" : 124,
        "name" : "Amelia",
        "city" : "Sydney",
        "phoneNo" : "1234567890"
    },
    {
        "personID" : 125,
        "name" : "Emily",
        "city" : "Perth",
        "phoneNo" : "1234567890"
    },
    {
        "personID" : 126,
        "name" : "Abraham",
        "city" : "Perth",
        "phoneNo" : "1234567890"
    }
];

function createElement(typeElement, value) {
    let element = document.createElement(typeElement);
    element.innerHTML = value;
    return element;
}

function createRow(element, array, table){
    array.forEach( rowInfo => {
        let row = document.createElement('tr');
        let id = createElement(element, rowInfo.personID);
        let name = createElement(element, rowInfo.name);
        let city = createElement(element, rowInfo.city);
        let phone = createElement(element, rowInfo.phoneNo);
        row.appendChild(id);
        row.appendChild(name);
        row.appendChild(city);
        row.appendChild(phone);
        table.appendChild(row);
    });
}

function showAttributes() {
    alert(`Elmento Seleccionado: ${this.textContent}\n
           "ID elemento: {${this.getAttribute('id')}}"
           "ISO iD: {${this.getAttribute('data-id')}}"
           "Dial Code: {${this.getAttribute('data-dial-code')}}"`);
}

crear.addEventListener('click', () => {
    if ( !divTable.hasChildNodes() ) {
        let table = document.createElement('table');
        createRow('th', header, table); 
        createRow('td', personArray, table);
        divTable.appendChild(table);
    }
});

eliminar.addEventListener('click', () => {
    if ( divTable.hasChildNodes() ) {
        divTable.removeChild(divTable.firstChild)
    }
});

for (let i = 0; i < list.length; i++) {
    list[i].addEventListener('click', showAttributes, false);
}
