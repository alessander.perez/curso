let out = document.getElementById('out');
let calc = document.getElementById('calc');
let operation = '';

calc.addEventListener('click', add, false)

function add(e) {
    if ( e.target.textContent == '=' ) result();
    else if ( e.target.textContent == 'AC' ) reset();
    else {
        if (e.target.textContent === '%' || e.target.textContent === '+/-') return;
        e.target.textContent === 'x' ? operation+='*' : operation += e.target.textContent;
        out.value = operation;
    }
}

function result() {
    let mathExpression = out.value;
    let result = eval(mathExpression);
    out.value = result;
    operation = result + '';
}

function reset() {
    operation = '';
    out.value = '0';
}