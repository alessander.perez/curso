function ejercicio1() {
    var anObject = {
        foo: 'bar',
        length: 'interesting',
        '0': 'bar',
        '1': 'one!'
    };
    var anArray = ['zero', 'one.'];
    console.log(anArray[0], anObject[0]);
    console.log(anArray[1], anObject[1]);
    console.log(anArray.length, anObject.length);
    console.log(anArray.foo, anObject.foo); 
    console.log(typeof anArray == 'object', typeof anObject == 'object');
    console.log(anArray instanceof Object, anObject instanceof Object);
    console.log(anArray instanceof Array, anObject instanceof Array);
}

function ejercicio2(){
    var obj = {
        a: "hello",
        b: "this is",
        c: "javascript!"
    };
    let objArray = document.getElementById('ejercicio2');
    console.log(Object.values(obj));
    objArray.innerHTML = Object.values(obj);
}

function ejercicio3() {
    let array = [];
    for (let i=0; i<99; i+=2) {
        console.log(i);
        array.push(i);
    }
    let element = document.getElementById('ejercicio3');
    element.innerHTML = array;
}

function ejercicio4() {
    let zero = 0;
    let array = [];
    function multiply(x) {
        return x*2;
    }
    function add(a = 1 + zero, b = a, c = b + a, d = multiply(c)) {
        console.log((a+b+c), d);
        array.push((a+b+c), d);
    }
    add(1);
    add(3);
    add(2,7);
    add(1,2,5);
    add(1,2,5,10);
    let element = document.getElementById('ejercicio4');
    element.innerHTML = array;
}

function ejercicio5(){
    class MyClass {
        constructor() {
            this.names_=[];
        }
        set name(value) {
            this.names_.push(value);
        }
        get name() {
            return this.names_[this.names_.length - 1];
        }
    }

    const myClassInstance = new MyClass();
    myClassInstance.name = 'Joe';
    myClassInstance.name = 'Bob';

    console.log(myClassInstance.name);
    console.log(myClassInstance.names_);
}

function ejercicio6(){
    const classInstance = new class {
        get prop() {
            return 5;
        }
    }

    classInstance.prop = 10;
    console.log(classInstance.prop);
}

function ejercicio7(){
    class Queue {
        constructor() {
            const list =[];
            this.enqueue = function (type) {
                list.push(type);
                return type;
            };
            this.dequeue = function() {
                return list.shift();
            };
        }
    }

    var q = new Queue();
    q.enqueue(9);
    q.enqueue(8);
    q.enqueue(7);

    console.log(q.dequeue());
    console.log(q.dequeue());
    console.log(q.dequeue());
    console.log(q);
    console.log(Object.keys(q));
}

function ejercicio8() {
    class Person {
        constructor(firstName, lastName){
            this._firstName = firstName;
            this._lastName = lastName;
        }

        set firstName(value) {
            this._firstName = value;
        }
        set lastName(value) {
            this._lastName = value;
        }
        get firstName() {
            return this._firstName;
        }
        get lastName() {
            return this._lastName;
        }
    }
    let person = new Person('John', 'Doe');
    person.firstName = 'Foo';
    person.lastName = 'Bar';
    console.log(person.firstName, person.lastName);
}

function ejercicio9() {
    var deleteBtn = document.querySelectorAll("[data-deletepost]");
    let padre = document.getElementById('post');

    deleteBtn[0].addEventListener('click', deletePost);
    deleteBtn[1].addEventListener('click', deletePost);

    function deletePost() {
        padre.removeChild(this.parentNode);
    }
}

function ejercicio10(){
    let sec = document.getElementById('s');
    let min = document.getElementById('m');
    let hrs = document.getElementById('h');
    let startstop = document.getElementById('startstop');
    let reset = document.getElementById('reset');
    let s = 0;
    let m = 0;
    let h = 0;

    startstop.addEventListener('click', startStop);
    reset.addEventListener('click', resetFunc);

    function startStop() {
        if ( startstop.textContent === 'Iniciar') {
            startstop.textContent = 'Parar';
            timerContinue();
        } else if ( startstop.textContent === 'Parar') {
            startstop.textContent = 'Iniciar';
            clearTimeout(t);
        }
    }

    function timerContinue() {
        if ( s < 10 ) sec.textContent = '0' + s;
        else sec.textContent = s;
        if ( m < 10 ) min.textContent = '0' + m;
        else min.textContent = m;
        if ( h < 10 ) hrs.textContent = '0' + h;
        else hrs.textContent = h;
        s++;
        while (s > 59) {
            s -= 60;
            m++;
        }
        while(m > 59) {
            m -= 60;
            h++;
        }
        t = setTimeout(timerContinue, 1000);
    }

    function resetFunc() {
        s = 0;
        m = 0;
        h = 0;
        hrs.textContent = '00';
        min.textContent = '00';
        sec.textContent = '00';
    }

}

ejercicio1();
ejercicio2();
ejercicio3();
ejercicio4();
ejercicio5();
ejercicio6();
ejercicio7();
ejercicio8();
ejercicio9();
ejercicio10();