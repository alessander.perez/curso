var req = new XMLHttpRequest();

req.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var myArr = JSON.parse(this.responseText);
      myFunction(myArr);
    }
};
  
function myFunction(arr) {
    let element = document.getElementById('container');
    element.textContent = arr;
    var out = '';
    for (let [key, value] of Object.entries(arr)) {
        out += `<a>${key}: ${value}</a><br>`;
        console.log(`${key}: ${value}`);
    } 
    element.innerHTML = out;
}

req.open('GET', 'https://api.wheretheiss.at/v1/satellites/25544', false); 
req.send(null);