import { html, css, LitElement } from 'lit-element';
import {classMap} from 'lit-html/directives/class-map';
import {styleMap} from 'lit-html/directives/style-map';

export class DynamicStyle extends LitElement {
  static get styles() {
    return css`
      .mydiv {
        background-color: blue;
      }
      .someclass {
        border: 1px solid red;
      }
    `;
  }

  static get properties() {
    return {
      Classes: { type: Object },
      styles: { type: Object },
    };
  }

  constructor() {
    super();
    this.Classes = {mydiv: true, someclass: true};
    this.styles = {color: 'green', fontFamily: 'Roboto'};
  }

  render() {
    return html`
      <div class=${classMap(this.Classes)} style=${styleMap(this.styles)}>
        Some content
      </div>
    `;
  }
}
