import { html, css, LitElement } from 'lit-element';

export class EContactList extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
      contactos: Array,
    };
  }

  constructor() {
    super();
    this.contactos = [
      {
        nombre: 'Lucho Godinez',
        email: 'user1_some_email@mail.com',
      },
      {
        nombre: 'Hugo Sanchez',
        email: 'user2_some_email@mail.com',
      },
      {
        nombre: 'John Doe',
        email: 'user3_some_email@mail.com',
      },
    ];
  }

  render() {
    return html`
      <div>
        ${this.contactos.map(
          contact =>
            html`<e-contact
              nombre="${contact.nombre}"
              email="${contact.email}"
            ></e-contact>`
        )}
      </div>
    `;
  }
}
customElements.define('e-contact-list', EContactList);
