import { LitElement, html } from 'lit-element';
import './my-text-input.js'

export class InputSampler extends LitElement {
    static get properties() {
        return {
            miDato: {type: String}
        }
    }

    constructor() {
        super();
        this.miDato = 'Valor de inicialización';
    }

    render() {
        return html`
        <p>Soy mi element</p>
        <my-text-input .value=${this.miDato} @change="${this.inputCambiado}"></my-text-input>
        <p>El dato escrito es ${this.miDato}</p> 
        <button @click="${this.resetTexto}">Borrar Texto</button>`;
    }
    inputCambiado(e) {
        this.miDato = e.detail;
    }
    resetTexto() {
        this.miDato = '';
    }
}
customElements.define('input-sampler', InputSampler);