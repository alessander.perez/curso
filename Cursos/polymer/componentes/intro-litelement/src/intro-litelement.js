import {LitElement, html} from 'lit-element';

export class IntroLitElement extends LitElement {
    render() {
        return html`
        <p>Soy Intro LitElement</p>`
    };
}

customElements.define('intro-litelement', IntroLitElement);