import {LitElement, html, css} from 'lit-element';

export class IntroLitElement extends LitElement {
    static get styles() {
        return css`
            :host {
                display: block;
                padding: 25px;
                color: var(--litelement-templates-text-color, #000);
            }
        `;
    }
    static get properties() {
        return {
            title: {
                type: String
            },
            counter: {
                type: Number
            },
            message: String,
            myString: String,
            myArray: Array,
            myBool: Boolean
        }

    } 

    constructor() {
        super();
        this.title = 'Heey there';
        this.counter = 5;
        this.myString = 'Hello World';
        this.myArray = ['an', 'array', 'of', 'test', 'data'];
        this.myBool = true;
        this.message = 'Loading';
        this.addEventListener('stuff-loaded', (e) =>{
            this.message = e.detail;
        });
        this.loadStuff();
    }
    render() {
        return html`
            <p>${this.message}</p>
            <h2>${this.title} Nr. ${this.counter}!</h2>
            <button @click=${this._increment}>increment</button>
            <hr>
            <p>array loops and conditionals</p>
            <p>${this.myString}</p>
            <ul>
                ${this.myArray.map(i => html`<li>${i}</li>`)}
            </ul>
            ${this.myBool ? html`<p> Render some Html if myBool is true</p>`:
            html`<p> Render some Html if myBool is false</p>`}
        `;
    };
    loadStuff(){
        this.dispatchEvent(new CustomEvent('stuff-loaded', {detail: 'Hello'}));
    }
    _increment() {
        this.counter++;
    }
}

customElements.define('otro-litelement', IntroLitElement);