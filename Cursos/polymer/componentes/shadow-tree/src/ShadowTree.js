import { html, css, LitElement } from 'lit-element';

export class ShadowTree extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        padding: 25px;
        color: var(--shadow-tree-text-color, #000);
      }
      * { color: red}
      p {
        font-family: sans-serif;
      }
      .myclass {
        margin: 100px;
      }
      #main {
        padding: 30px;
      }
      h1 {
        font-size: 4em;
      }
    `;
  }

  static get properties() {
    return {
      title: { type: String },
      counter: { type: Number },
    };
  }

  constructor() {
    super();
    this.title = 'Hey there';
    this.counter = 5;
  }

  __increment() {
    this.counter += 1;
  }

  render() {
    return html`
      <p>Lorem ipsum</p>
      <p class="myclass">Parrafo 1</p>
      <p id="main">Parrafo 2</p>
      <h1>T i t u l o</h1>
    `;
  }
}
