import { html, css, LitElement } from 'lit-element';

export class SuperElemento extends LitElement {
  static get styles() {
    return css`
      button {
        width: 300px;
        font-style: italic;
      }
    `;
  }

  static get properties() {
    return {
      title: { type: String },
      counter: { type: Number },
    };
  }

  constructor() {
    super();
    this.title = 'Hey there';
    this.counter = 5;
  }

  __increment() {
    this.counter += 1;
  }

  render() {
    return html`
      <button >click</button>
    `;
  }
}
