import { css } from 'lit-element';
import {SuperElemento} from './SuperElemento'

export class HerenciaElemento extends SuperElemento {

    static get styles() {
        return [
            super.styles,
            css`button {color: red}`
        ]
    }

}

window.customElements.define('herencia-elemento', HerenciaElemento);
