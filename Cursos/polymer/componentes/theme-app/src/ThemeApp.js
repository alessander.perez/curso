import { html, css, LitElement } from 'lit-element';

export class ThemeApp extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        padding: 25px;
        color: var(--my-element-text-color, #000);
        background: var(--my-element-background-color, white);
        font-family: var(--my-element-font-family, Roboto);
        color: var(--theme-app-text-color, #000);
      }
      :host([hidden]) {
        display: none;
      }
    `;
  }

  static get properties() {
    return {
      title: { type: String },
      counter: { type: Number },
    };
  }

  constructor() {
    super();
    this.title = 'Hey there';
    this.counter = 5;
  }

  render() {
    return html`
      <p><div>Lorem ipsum dolor sir,dssdju, conbdjhsh djsdh ......</div></p>
    `;
  }
}
