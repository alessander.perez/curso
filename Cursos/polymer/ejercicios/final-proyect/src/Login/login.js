import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/paper-listbox/paper-listbox.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/paper-input/paper-input.js';
import '@polymer/iron-form/iron-form.js';
import { setPassiveTouchGestures, setRootPath } from '@polymer/polymer/lib/utils/settings.js';

setPassiveTouchGestures(true);
setRootPath(MyAppGlobals.rootPath);


class Login extends PolymerElement {
  static get template() {
    return html`
        <style>
            :host {
            display: block;

            padding: 10px;
            }

            .login {
                margin: auto;
                margin-top: 200px;
                width: 300px;
                height: 600px;
            } 
            .center {
                height: 100vh;
            }

            iron-form {
                text-align: center;
            }
        </style>
        <template is="dom-if" if="{{registerLogin}}" restamp id="form">
            <div class="center">
                <div class="login">
                    <iron-form id="form1">
                        <form action="/foo" method="get">
                            <paper-input type="text" name="name" required label="Usuario" value=""></paper-input>
                            <paper-input type="password" name="password" required label="Password" value=""></paper-input>
                            <br><br>
                            <paper-button type = "submit" raised on-click="login">Login</paper-button>
                            <paper-button type = "button" raised on-click="newUser">New User</paper-button>
                        </form>
                        <br>
                        <div class="output"></div>
                    </iron-form>
                </div>
            </div>
        </template>
        <template is="dom-if" if="{{!registerLogin}}" restamp>
            <div class="center">
                <div class="login">
                    <iron-form id="form2">
                        <form action="/foo" method="get">
                            <paper-input type="text" name="name" required label="Username" value=""></paper-input>
                            <paper-input type="text" name="password" required label="Password" value=""></paper-input>
                            <paper-input type="password" name="password" required label="Confirm Password" value=""></paper-input>
                            <br><br>
                            <paper-button raised type="button" on-click="cancel">Cancel</paper-button>
                            <paper-button type = "submit" raised on-click="addUser">Register</paper-button>
                        </form>
                        <br>
                        <div class="output"></div>
                    </iron-form>
                </div>
            </div>
        </template>
        
    `;
  }

  constructor() {
      super();
    
  }

  static get properties() {
    return {
      registerLogin: {
        type: Boolean,
        value: true,
        notify: true,
        observer: '_notification'
      },
      routeData: Object,
      subroute: Object
    };
  }

  connectedCallback() {
      super.connectedCallback();
      //this.registerLogin = false;
  }

  _notification(newvalue, oldvalue) {
      console.log(newvalue);
  }

  login(){
      console.log('redirecting');
  }

  newUser(e){
    console.log(e.target.parentNode);
    this.registerLogin = false;
    console.log(this.reegisterLogin);
    console.log('it change');
  }

  addUser(){
    
  }

  cancel(){
    this.registerLogin = true;
  }
}

window.customElements.define('view-login', Login);