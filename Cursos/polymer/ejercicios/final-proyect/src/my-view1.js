/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/paper-tabs/paper-tabs.js';
import '@polymer/paper-tabs/paper-tab.js';
import '@polymer/iron-collapse/iron-collapse.js';
import './shared-styles.js';

class MyView1 extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;
          padding: 10px;
          background: white;
        }

        paper-tabs {
          text-align: left;
          background: rgb(235,235,235);
        }
        .collapseDiv {
          clear: both;
          padding: 15px;
          background: rgb(240,240,240);
          border: 1px solid rgb(150,150,150);
          border-radius: 20px 20px 0 0;
          color: pink;
          
        }
        .collapseDiv button {
          float: right;
          clear: both;
          background: transparent;
          font-size: 25px;
          color: pink;
          border: none;
          outline: none;
        }
        iron-pages {
          padding: 20px;
        }
        .collapse {
          border: 1px solid rgb(150,150,150);
          border-top: none;
          border-radius: 0px 0px 20px 20px;
          
        }

        .form {
          padding: 15px;
        }
        .form div {
          display: inline-block;
          margin: 1%;
        }
        input {
          border-radius: 15px;
          height: 25px;
          width: 99%;
        }
        .twoThird {
          width: 72%;
        }
        .oneThird {
          width: 22%;
        }
        .middle {
          width: 47%; 
        }
        .third {
          width: 31%;
        }
        .grid-container {
          display: grid;
          grid-template-columns: auto auto auto;
          grid-gap: 10px;
        }
        
        .item1 {
          grid-column: 1 / 3;
        }

        .divGuardar {
          text-align: center;
        }

        .guardar {
          background: pink;
          color: white;
          font-size: 30px;
          outline: none;
        }
        
      </style>

      <paper-tabs selected="{{selected}}">
        <paper-tab>Mis datos</paper-tab>
        <paper-tab>Datos de acceso</paper-tab>
        <paper-tab>Mis intereses</paper-tab>
        <paper-tab>Sobre mi</paper-tab>
      </paper-tabs>

      <iron-pages selected="{{selected}}">
        <div>
          <form on-submit="guardarMisDatos">

            <div class="collapseDiv">
              Datos generales
              <button on-click="toggle" type="button" my-attribute="collapse1">&#8744</button>
            </div>
            <iron-collapse id="collapse1" class="collapse">
              <div class="form">
                <div class="middle"><label>Nombre Completo*</label></div>
                <div class="middle"><label>Apellidos*</label></div>
                <div class="middle"><input type="text" name="nombres" id="nombres" required></div>
                <div class="middle"><input type="text" name="apellidos" id="apellidos" required></div>
                <div class="third"><label>DNI*</label></div>
                <div class="third"><label>Telefono*</label></div>
                <div class="third"><label>Fecha de Nacimiento*</label></div>
                <div class="third"><input type="number" name="DNI" id="DNI" required></div>
                <div class="third"><input type="tel" id="tel" name="tel" required></div>
                <div class="third"><input type="date" id="date" name="date" required></div>
              </div>
            </iron-collapse>
            <br>

            <div class="collapseDiv">
              Domicilio Habitual
              <button on-click="toggle" type="button" my-attribute="collapse2">&#8744</button>
            </div>
            <iron-collapse id="collapse2" class="collapse">
              <div class="form grid-container">
                <div class="item1">
                  <label>Dirección*</label>
                  <input type="text" id="direccion">
                  <div class="middle"><label>Ciudad*</label></div>
                  <div class="middle"><label>Provincia*</label></div>
                  <div class="middle"><input type="text" id="ciudad"></div>
                  <div class="middle"><input type="text" id="provincia"></div>
                  <label>Exento de IVA*</label>
                  <input type="checkbox" id="nombres">
                </div>
                <div>
                  <label>Codigo Postal*</label>
                  <input type="number" id="postal">
                  <label>País*</label>
                  <input type="text" id="pais" value="México" disabled>
                  <label>Zona Horaria*</label>
                  <input list="zona">
                    <datalist id="zona">
                      <option value="Guadalajara">
                      <option value="China">
                      <option value="England">
                      <option value="Madrid">
                    </datalist>
                </div>
              </div>
            </iron-collapse>
            <br>

            <div class="collapseDiv">
              Datos Profesionales
              <button on-click="toggle" type="button" my-attribute="collapse3">&#8744</button>
            </div>
            <iron-collapse id="collapse3" class="collapse">
              <div class="form">
                <div class="middle"><label>¿Qué estudios tienes?</label></div>
                <div class="middle"><label>Compañia/Empresa</label></div>
                <div class="middle">
                <input list="estudios">
                <datalist id="estudios">
                  <option value="Universitarios">
                  <option value="Bachillerato">
                  <option value="Secundaria">
                  <option value="Primaria">
                </datalist>
                </div>
                <div class="middle"><input type="text" id="compania"></div>
              </div>
            </iron-collapse>
            <br>
            <div class="divGuardar"><button type="submit" class="guardar">Guardar Cambios</button></div>
          </form>          
        </div>

        <div>
          <form on-submit="datosAcceso">
            <div class="form">
              <label>Email*</label>
              <input type="text" id="email" value="alessander.sshc@kit.com" disabled>
              <div class="middle"><label>Contraseña nueva*</label></div>
              <div class="middle"><label>Contraseña de Confirmacion*</label></div>
              <div class="middle"><input type="text" name="new" id="newpasssword" required></div>
              <div class="middle"><input type="password" id="comfirmpassword" name="confirm" required></div>
            </div>  
            <div class="divGuardar">
              <button type="button" on-click="desactivarCuenta" class="guardar">Desactivar cuenta</button>
              <button type="submit" class="guardar">Guardar Cambios</button>
            </div>
          </form>        
        </div>

        <div>Page 3</div>

        <div>
          <form on-submit="sobreMi">
            <div class="form">
              <div class="middle"><label>Twitter Username*</label></div>
              <div class="middle"><label>Github Username*</label></div>
              <div class="middle"><input type="text" name="twitter" id="twitter" required></div>
              <div class="middle"><input type="password" id="github" name="github" required></div>
              <div class="middle"><label>Facebook url*</label></div>
              <div class="middle"><label>Linkedin url*</label></div>
              <div class="middle"><input type="text" name="face" id="face" required></div>
              <div class="middle"><input type="text" id="link" name="link" required></div>
              <label>website</label>
              <label>Bio</label>
              <textarea></textarea>
            </div>  
            <div class="divGuardar">
              <button type="submit" class="guardar">Guardar Cambios</button>
            </div>
          </form>        
        </div>
      </iron-pages>

    `;
  }
  static get properties() {
    return {
      selected: {
        type: String,
        reflectToAttribute: true
      },
      routeData: Object,
      subroute: Object
    };
  }

  toggle(e) {
    var id = e.target.getAttribute('my-attribute');
    this.shadowRoot.querySelector('#'+id).toggle();
  }

  guardarMisDatos(e) {
    e.preventDefault();
    console.log('everithing, ', e.target);
  }

  sobreMi(e) {
    e.preventDefault();
    console.log('sobreMi: ', e.target);

    
  }

  datosAcceso(e) {
    e.preventDefault();
    console.log('Datos de acceso ', e.target);
    console.log(this.$.newpasssword.value);
    console.log(this.$.comfirmpassword.value);
    if (this.$.newpasssword.value === this.$.comfirmpassword.value) {
      console.log('Se ha guardado tu nueva contraseña');
    } else {
      alert('No es posible guardar contraseña');
    }
  }

  desactivarCuenta() {
    console.log('Desactivada');
  }

}

window.customElements.define('my-view1', MyView1);
