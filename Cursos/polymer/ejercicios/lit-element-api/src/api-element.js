import { LitElement, html, css } from 'lit-element';

export class ApiElement extends LitElement {

    static get styles() {
        return css`
            :host {
                display: block;
            }
            table {
                width: 98%;
                border-collapse: collapse;
                margin: 1%;
            }
            th, td {
                padding: 8px;
                text-align: left;
                border-bottom: 1px solid #cfeefa;
                border-top: 1px solid #cfeefa;
            }
            tr:nth-child(n+2):nth-child(even) {
                background-color: #cfeefa;
            }
            .empleados {
                box-sizing: border-box;
                border: 1px solid rgb(17, 121, 153);
                clear: both;
                overflow: auto;
            }
            .title {
                padding: 10px;
                background-color: rgb(17, 121, 153);
                color: white;
            }
            .search {
                padding: 10px;
            }
            .right {
                float: right;
                margin: 10px;
            }
        `;
    }

    static get properties() {
        return {
            employees: Array
        }
    }

    constructor() {
        super();
        this.employees = [];
    }

    async connectedCallback() {
        super.connectedCallback();
        const list = await this.fillList();
        this.fillTable(list);
    }

    async fillList() {
        let list = await fetch('http://dummy.restapiexample.com/api/v1/employees');
        list = await list.json();
        return list.data;
    }

    fillTable(list) {
        list.forEach( element => {
            let name = element.employee_name.split(' ');
            this.employees = [...this.employees, (new Object({firstName: name[0], lastName: name[1], salary: element.employee_salary}))];
        });
    }

    filterTable(e) {
        let filter = e.target.value.toLowerCase(),
            bodyTable = this.shadowRoot.getElementById("bodyTable");
        Array.from(bodyTable.children).forEach( row => {
            let firstName = row.cells[0].textContent,
                lastName = row.cells[1].textContent;
            if (firstName.toLowerCase().indexOf(filter) > -1 || lastName.toLowerCase().indexOf(filter) > -1) {
                row.style.display = "";
            } else {
                row.style.display = "none";
            }
        });
    }

    render() {
        return html`
            <div class="empleados">
                <div class="title">View: SimplePage</div>
                <div class="search">
                    <label>Filter: </label>
                    <input type="search" id="key" @input="${this.filterTable}">
                </div>
                <table> 
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Last Name</th>
                            <th>Amount (YEN)</th>
                        </tr>
                    </thead>
                    <tbody id="bodyTable">
                        ${this.employees.map(item => 
                            html `
                                <tr>
                                    <td>${item.firstName}</td>
                                    <td>${item.lastName}</td>
                                    <td>${item.salary}</td>
                                </tr>
                            `
                        )}
                    </tbody>
                </table>
                <button class="right">OK</button>
                <button class="right">Cancel</button>
            </div>
        `;
    }
}
customElements.define('api-element', ApiElement);