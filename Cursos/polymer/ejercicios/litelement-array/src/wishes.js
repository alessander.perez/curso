import {LitElement, html, css} from 'lit-element';
import { styleMap } from 'lit-html/directives/style-map';
import { classMap } from 'lit-html/directives/class-map';

class WhishesArray extends LitElement {
    static get properties() {
        return {
            wishesArray: { type: Array },
            ids: Number
        }
    }

    constructor() {
        super();
        this.changeStatusColor = this.changeStatusColor.bind(this);
        this.wishesArray = [];
        this.ids=0;
    }

    static get styles() {
        return css`
            .wishes {
                display: flex;
                justify-content: center;
                align-items: center;
                flex-direction: column;
            }
            ul {
                margin: 10px 0;
            }
            .good {
                color: green;
            }
            .alert {
                color: yellow;
            }
            .warning {
                color: red;
            }
            .done {
                text-decoration:line-through;
                color: black !important; 
            }
        `;
    }

    render() {
        return html`
            <h1>My Whishlist</h1>
            <fieldset>
                <legend>New Wish</legend>
                <input type="text" id="input">
                <button @click=${this.addWish}>Add wish</button>
            </fieldset>
            <div class="wishes">
                <ul id="wishes">
                    ${this.wishesArray.map(wish => 
                        html `
                                <li class=${classMap(wish.classes)} id="${wish.id}">
                                    <input type="checkbox" ?checked="${wish.checked}" @change="${this.wishDone}"> ${wish.value}
                                </li>
                        `
                    )}
                </ul>
            </div>
            <button @click=${this.deleteWishes}>Archive Done</button>
        `;
    }

    wishDone(e) {
        let index = this.wishesArray.findIndex( wish => e.path[1].id == wish.id);
        this.wishesArray[index].checked = !this.wishesArray[index].checked;
        e.path[0].checked ? this.wishesArray[index].classes.done = true : this.wishesArray[index].classes.done = false;
        this.wishesArray = [...this.wishesArray];
    }


    deleteWishes() {
        let wishes = this.shadowRoot.getElementById('wishes');
        let inputs = wishes.querySelectorAll('input');
        if (inputs) {
            inputs.forEach( wish => {
                if (wish.checked) {
                    this.wishesArray.splice( this.wishesArray.findIndex( object => wish.parentNode.id == object.id ), 1 );
                    this.wishesArray = [...this.wishesArray];
                }
            });
        }
    }

    addWish() {
        let inputElement = this.shadowRoot.getElementById('input');
        if (inputElement.value) {
            this.wishesArray = [...this.wishesArray, new Object({value: inputElement.value, checked: false, classes: {good: true}, id: this.ids})];
            setTimeout( this.changeStatusColor, 5000, this.ids++, 'yellow');
            inputElement.value = '';
        }
    }



    changeStatusColor(id, color) {
        this.wishesArray.forEach( wish => {
            if ( wish.id == id ) {
                switch(color) {
                    case 'yellow': 
                        wish.classes.alert = true;
                        this.wishesArray = [...this.wishesArray];
                        setTimeout( this.changeStatusColor, 5000, id, 'red');
                        break;
                    case 'red':
                        wish.classes.warning = true;
                        this.wishesArray = [...this.wishesArray];
                        break;
                }
            }
        });
    }

}

customElements.define('wishes-array', WhishesArray);