class ClickMe extends HTMLElement{
    constructor(){
        super();
        this.addEventListener('click', function(e) {
            alert('hola');
        });
        
    }
}

customElements.define('click-me', ClickMe);
customElements.define('click-me-2', class extends HTMLElement{
    constructor(){
        super();
        this.addEventListener('click', function(e) {
            alert('hola');
        });
        
    }
});

class MiBotonExtendido extends HTMLButtonElement {
    constructor(){
        super();
        this.addEventListener('click', () => {
            console.log("evento click ", this.innerHTML);
            alert('Mi boton extendido');
        });
    }

    static get ceName () {
        return 'mi-boton-extendido';
    }

    get is(){
        return this.getAttribute('is');
    }

    set is (value) {
        this.setAttribute('is', value || this.ceName);
    }
}

customElements.define('mi-boton-extendido', MiBotonExtendido, {extends: 'button'});

const MiBotonExtendido2 = document.createElement('button', {is:MiBotonExtendido.ceName});
MiBotonExtendido2.textContent = 'Soy un boton';
document.body.appendChild(MiBotonExtendido2);
const MiBotonExtendido3 = document.createElement('button', {is:MiBotonExtendido.ceName});
MiBotonExtendido3.textContent = 'Hola boton 3';
document.querySelector('#container').appendChild(MiBotonExtendido3);