class MiMensaje extends HTMLElement {
    constructor() {
        super();
        this.addEventListener('click', () => {
            alert('Click en menssaje');
        });
    }
    static get observedAttributes(){
        return ['msj', 'casi-visible'];
    }

    connectedCallback(){
        console.log('connected');
    }

    disconnectedCallback() {
        alert('desconnected');
    }

    attributeChangedCallback (attrName, oldVal, newVal) {
        console.log('attributes');
        if ( attrName === 'msj' ){
            this.pintarMensaje(newVal);
        }
        if(attrName === 'casi-visible'){
            this.setCasiVisible();
        }
    }
    pintarMensaje(msj) {
        this.innerHTML = msj;
    }

    get msj() {
        return this.getAttribute('msj');
    }

    set msj(val) {
        this.setAttribute('msj', val);
    }

    get casiVisible() {
        return this.hasAttribute('casi-visible');
    }

    set casiVisible(value) {
        if(value){
            this.setAttribute('casi-visible', '');
        } else {
            this.removeAttribute('casi-visible');
        }
    }

    setCasiVisible() {
        if(this.casiVisible) {
            this.style.opacity = 0.1;
        } else {
            this.style.opacity = 1;
        }
    }
}

customElements.define('mi-mensaje', MiMensaje);

let miMensaje = document.createElement('mi-mensaje');
miMensaje.msj = 'Otro mensaje';
document.body.appendChild(miMensaje);

let tercerMensaje = new MiMensaje();
tercerMensaje.msj = 'Tercer mensaje';
document.body.appendChild(tercerMensaje);