class Search extends HTMLElement {

    constructor() {
        super();
    }

    connectedCallback(){
        let searchButton = document.getElementById('search'),
            clear = document.getElementById('clear');

        searchButton.addEventListener('click', this.searchEmployee);
        clear.addEventListener('click', this.clearTable);
    }

    clearTable() {
        let tr = document.getElementsByTagName("tr"),
            searchInput = document.getElementById('key');
        searchInput.value = '';
        for (let i = 0; i < tr.length; i++) {
            tr[i].style.display = "";
        }
    }

    searchEmployee() {
        let searchInput = document.getElementById('key'),
            filter = searchInput.value.toUpperCase(),
            table = document.getElementById("employes"),
            tr = table.getElementsByTagName("tr");

        for (let i = 0; i < tr.length; i++) {
            let td = tr[i].getElementsByTagName("td") ; 
            for(let j=0 ; j<td.length ; j++)
            {
                let tdata = td[j];
                if (tdata) {
                    if (tdata.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                        break ; 
                    } else {
                        tr[i].style.display = "none";
                    }
                } 
            }
        }
    }
}

class Table extends HTMLElement {

    constructor() {
        super();
    }

    async connectedCallback(){
        await this.fillList();
        this.fillTable();
    }

    async fillList() {
        this.list = await fetch('http://dummy.restapiexample.com/api/v1/employees');
        this.list = await this.list.json();
        this.list = this.list.data;
    }

    fillTable() {
        let table = document.getElementById('employes');
        this.list.forEach( element => {
            let name = element.employee_name.split(' '),
                salary = element.employee_salary,
                tr = document.createElement('tr');
            this.addTr(tr, name[0]);
            this.addTr(tr, name[1]);
            this.addTr(tr, salary);
            tr.id = element.id;
            table.appendChild(tr);
        });
    }

    addTr(tr, value) {
        let td = document.createElement('td');
        td.textContent = value;
        tr.appendChild(td);
    }
}



customElements.define('search-component', Search);
customElements.define('table-component', Table);
