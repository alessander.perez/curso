class TodoApp extends HTMLElement {
    constructor() {
        super();
        this._timesClicked = 0;
    
        var button = document.createElement("button");
        button.textContent = "Click me";
        button.onclick = (evt) => {
            this._timesClicked++;
            this.dispatchEvent(new CustomEvent("clicked", {
                bubbles: true,  // bubble event to containing elements
                composed: true,
                detail: this._timesClicked
            }));
        };
        this.append(button);
    }

}

class Data extends HTMLElement {
    constructor() {
        super();
        /*var counter = document.querySelector("container-data");
        counter.addEventListener("clicked", (evt) => {
            console.log(evt.detail);
        });

        /*con esto no funciona XD */
        document.body.addEventListener('clicked', (event) => {
            console.log('waht')
            const { amount, data } = event.detail;
            console.log(data); 
            this.innerHTML = data;
        });
    }

}


window.customElements.define('container-data', TodoApp);
window.customElements.define('data-show', Data);